<?php

namespace Weeny\Core\ContainerLoader;

use Weeny\Contract\Container\ServiceLocatorInterface;

class ServiceLocatorStack extends AbstractStack implements ServiceLocatorInterface
{

    public function addServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->addElement($serviceLocator);
    }

    /**
     * @inheritDoc
     */
    public function has($serviceName): bool
    {
        return $this->hasElement($serviceName);
    }

    /**
     * @inheritDoc
     */
    public function get($serviceName)
    {
        return $this->getElement($serviceName);
    }
}