<?php
namespace Weeny\Core\ContainerLoader\Exceptions;

use Weeny\Contract\Container\Exceptions\PackagesNotContainContainerBuilderExceptionInterface;

class PackagesNotContainContainerBuilderException extends \Exception implements PackagesNotContainContainerBuilderExceptionInterface
{

}