<?php

namespace Weeny\Core\ContainerLoader\Exceptions;

use Throwable;
use Weeny\Contract\Container\Exceptions\ServiceNotFoundExceptionInterface;

class ServiceNotFoundException extends \Exception implements ServiceNotFoundExceptionInterface
{

    protected $serviceName;

    public function __construct(string $serviceName, $message = "", $code = 0, Throwable $previous = null)
    {
        $this->serviceName = $serviceName;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @inheritDoc
     */
    public function getServiceName(): string
    {
        return $this->serviceName;
    }
}