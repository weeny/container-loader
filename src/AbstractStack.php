<?php

namespace Weeny\Core\ContainerLoader;

use Weeny\Core\ContainerLoader\Exceptions\ServiceNotFoundException;

abstract class AbstractStack
{

    protected $containers = [];
    protected $serviceToIndex = [];

    protected function addElement($container) {
        if ( in_array($container, $this->containers) ) {
            return;
        }
        $this->containers[] = $container;
    }

    /**
     * @inheritDoc
     */
    protected function getElement($id)
    {
        if ( array_key_exists($id, $this->serviceToIndex) ) {
            if ( $this->containers[$this->serviceToIndex[$id]]->has($id) ) {
                return $this->containers[$this->serviceToIndex[$id]]->get($id);
            }
            unset($this->serviceToIndex[$id]);
        }

        foreach ($this->containers as $index => $container) {
            if ( $container->has($id) ) {
                $this->serviceToIndex[$id] = $index;
                return $container->get($id);
            }
        }

        throw new ServiceNotFoundException(
            $id,
            sprintf('Service %s not found in container', $id)
        );
    }

    /**
     * @inheritDoc
     */
    protected function hasElement($id)
    {
        if ( array_key_exists($id, $this->serviceToIndex) ) {
            if ( $this->containers[$this->serviceToIndex[$id]]->has($id) ) {
                return true;
            }
            unset($this->serviceToIndex[$id]);
        }

        foreach ($this->containers as $index => $container) {
            if ( $container->has($id) ) {
                $this->serviceToIndex[$id] = $index;
                return true;
            }
        }
        return false;
    }
}