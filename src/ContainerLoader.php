<?php
namespace Weeny\Core\ContainerLoader;

use Psr\Container\ContainerInterface;
use Weeny\Contract\Container\ContainerLoaderInterface;
use Weeny\Contract\Container\ExternalContainerBuilderInterface;
use Weeny\Contract\Package\ContainerBuilderPackageInterface;
use Weeny\Contract\Package\PackageCollectionInterface;
use Weeny\Core\ContainerLoader\Exceptions\PackagesNotContainContainerBuilderException;

class ContainerLoader implements ContainerLoaderInterface
{

    /**
     * @inheritDoc
     */
    public function loadFromPackagesConfiguration(PackageCollectionInterface $pakages): ContainerInterface {
        /**
         * @var ContainerBuilderPackageInterface[]|PackageCollectionInterface
         */
        $pakagesWithBuilder = $pakages->filterInstanceOf(ContainerBuilderPackageInterface::class);

        if ($pakagesWithBuilder->count() == 0) {
            throw new PackagesNotContainContainerBuilderException('Container is not be loaded without ContainerBuilder objects');
        }

        /**
         * @var ExternalContainerBuilderInterface[] $containerBuilders
         */
        $containerBuilders = [];
        foreach ($pakagesWithBuilder as $package) {
            /**
             * @var ContainerBuilderPackageInterface $package
             */
            $containerBuilders[] = $package->loadExternalContainerBuilder($pakages);
        }

        $serviceLocator = new ServiceLocatorStack();
        foreach ($containerBuilders as $containerBuilder) {
            $serviceLocator->addServiceLocator($containerBuilder->configure());
        }

        $container = new ContainerStack();
        foreach ($containerBuilders as $containerBuilder) {
            $container->addContainer($containerBuilder->build($serviceLocator));
        }
        return $container;
    }
}