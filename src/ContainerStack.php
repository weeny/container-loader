<?php

namespace Weeny\Core\ContainerLoader;

use Psr\Container\ContainerInterface;

class ContainerStack extends AbstractStack implements ContainerInterface
{

    public function addContainer(ContainerInterface $container) {
        $this->addElement($container);
    }

    /**
     * @inheritDoc
     */
    public function get($id)
    {
        return $this->getElement($id);
    }

    /**
     * @inheritDoc
     */
    public function has($id)
    {
        return $this->hasElement($id);
    }
}