<?php
namespace Weeny\Core\ContainerLoader\Tests;

use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Weeny\Contract\Container\Exceptions\ServiceNotFoundExceptionInterface;
use Weeny\Core\ContainerLoader\ContainerStack;
use Weeny\Core\ContainerLoader\Tests\Fixture\SimpleContainer;

class ContainerStackTest extends TestCase
{

    /**
     * @dataProvider dataProviderForHas
     */
    public function testContainer(ContainerInterface $container, string $serviceId, $expectedHas, $expectedService = null) {
        $this->assertEquals($expectedHas, $container->has($serviceId));
        if (!$expectedHas) {
            return;
        }
        $this->assertEquals($expectedService, $container->get($serviceId));
    }

    public function dataProviderForHas() {
        $serviceOne = new \stdClass();
        $serviceTwo = new \DateTime();
        $container = new ContainerStack();
        $containerOne = new SimpleContainer();
        $containerOne->add('ServiceOne', $serviceOne);
        $containerTwo = new SimpleContainer();
        $containerTwo->add('ServiceTwo', $serviceTwo);
        $container->addContainer($containerOne);
        $container->addContainer($containerTwo);
        return [
            [$container, 'ServiceOne', true, $serviceOne],
            [$container, 'ServiceTwo', true, $serviceTwo],
            [$container, 'ServiceThree', false],
        ];
    }

    public function testHasAfterRemove() {
        $collected = new ContainerStack();
        $this->assertFalse($collected->has('ServiceOne'));

        $container = new SimpleContainer();
        $container->add('ServiceOne', true);
        $collected->addContainer($container);

        $this->assertTrue($collected->has('ServiceOne'));
        $this->assertTrue($collected->has('ServiceOne'));

        $container->remove('ServiceOne');

        $this->assertFalse($collected->has('ServiceOne'));
    }

    public function testGetAfterRemove() {
        $collected = new ContainerStack();

        try {
            $this->assertFalse($collected->get('ServiceOne'));
            $this->fail('Exceptions will not be throwed');
        } catch (ServiceNotFoundExceptionInterface $e) {

        }

        $container = new SimpleContainer();
        $container->add('ServiceOne', true);
        $collected->addContainer($container);

        $this->assertTrue($collected->get('ServiceOne'));
        $this->assertTrue($collected->get('ServiceOne'));

        $container->remove('ServiceOne');

        try {
            $this->assertFalse($collected->get('ServiceOne'));
            $this->fail('Exceptions will not be throwed');
        } catch (ServiceNotFoundExceptionInterface $e) {

        }
    }

    public function testDobleAdd() {
        $collected = new ContainerStack();
        $container = new SimpleContainer();
        $container->add('ServiceOne', true);
        $collected->addContainer($container);
        $collected->addContainer($container);
        $this->assertTrue($collected->get('ServiceOne'));

    }
}