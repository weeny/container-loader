<?php
namespace Weeny\Core\ContainerLoader\Tests\Fixture;

use Psr\Container\ContainerInterface;
use Weeny\Core\ContainerLoader\Exceptions\ServiceNotFoundException;

class SimpleContainer implements ContainerInterface
{

    private $services = [];

    public function add(string $service, $value)
    {
        $this->services[$service] = $value;
    }

    public function remove(string $service)
    {
        if ( !$this->has($service) ) {
            return;
        }

        unset($this->services[$service]);
    }

    /**
     * @inheritDoc
     */
    public function get($id)
    {
        if ( !$this->has($id) ) {
            throw new ServiceNotFoundException($id, 'Service not found');
        }

        return $this->services[$id];
    }

    /**
     * @inheritDoc
     */
    public function has($id)
    {
        return array_key_exists($id, $this->services);
    }
}