<?php

namespace Weeny\Core\ContainerLoader\Tests\Fixture;

use Weeny\Contract\Container\ServiceLocatorInterface;
use Weeny\Core\ContainerLoader\Exceptions\ServiceNotFoundException;

class SimpleLocator implements ServiceLocatorInterface
{

    private $services = [];

    public function add(string $service, $value)
    {
        $this->services[$service] = $value;
    }

    public function remove(string $service)
    {
        if ( !$this->has($service) ) {
            return;
        }

        unset($this->services[$service]);
    }

    /**
     * @inheritDoc
     */
    public function has($serviceName): bool
    {
        return array_key_exists($serviceName, $this->services);
    }

    /**
     * @inheritDoc
     */
    public function get($serviceName)
    {
        if ( !$this->has($serviceName) ) {
            throw new ServiceNotFoundException($serviceName, 'Service not found');
        }

        return $this->services[$serviceName];
    }
}