<?php

namespace Weeny\Core\ContainerLoader\Tests;

use PHPUnit\Framework\TestCase;
use Weeny\Contract\Container\Exceptions\ServiceNotFoundExceptionInterface;
use Weeny\Core\ContainerLoader\ServiceLocatorStack;
use Weeny\Core\ContainerLoader\Tests\Fixture\SimpleLocator;

class ServiceLocatorStackTest extends TestCase
{

    public function getNegative() {
        $this->expectException(ServiceNotFoundExceptionInterface::class);
        $stack = new ServiceLocatorStack();
        $stack->getService('someService');
    }

    public function testHasService() {
        $stack = new ServiceLocatorStack();
        $this->assertFalse($stack->has('someService'));
        $serviceLocator = new SimpleLocator();
        $serviceLocator->add('someService', new \stdClass());
        $stack->addServiceLocator($serviceLocator);
        $this->assertTrue($stack->has('someService'));
    }

    public function testGetService() {
        $stack = new ServiceLocatorStack();
        $serviceLocator = new SimpleLocator();
        $service = new \stdClass();
        $serviceLocator->add('someService', $service);
        $stack->addServiceLocator($serviceLocator);
        $this->assertEquals($service, $stack->get('someService'));
    }
}