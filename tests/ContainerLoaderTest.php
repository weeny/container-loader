<?php

namespace Weeny\Core\ContainerLoader\Tests;

use PHPUnit\Framework\TestCase;
use Weeny\Contract\Container\ExternalContainerBuilderInterface;
use Weeny\Contract\Container\ServiceLocatorInterface;
use Weeny\Contract\Package\ContainerBuilderPackageInterface;
use Weeny\Contract\Package\PackageInterface;
use Weeny\Core\ContainerLoader\ContainerLoader;
use Weeny\Core\ContainerLoader\Exceptions\PackagesNotContainContainerBuilderException;
use Weeny\Core\PackageManager\Collection\PackageCollection;

class ContainerLoaderTest extends TestCase
{

    public function testWithoutBuilder() {
        $this->expectException(PackagesNotContainContainerBuilderException::class);
        $loader = new ContainerLoader();
        $loader->loadFromPackagesConfiguration(new PackageCollection());
    }

    public function testLoadContainer() {
        $packageOne = $this->getMockBuilder(ContainerBuilderPackageInterface::class)->getMock();
        $packageTwo = $this->getMockBuilder(ContainerBuilderPackageInterface::class)->getMock();
        $packageThree = $this->getMockBuilder(PackageInterface::class)->getMock();

        $packages = new PackageCollection($packageOne, $packageTwo, $packageThree);

        $builderOne = $this->getMockBuilder(ExternalContainerBuilderInterface::class)->getMock();
        $packageOne
            ->expects($this->once())
            ->method('loadExternalContainerBuilder')
            ->with($packages)
            ->willReturn($builderOne);

        $serviceLocatorOne = $this->getMockBuilder(ServiceLocatorInterface::class)->getMock();
        $serviceLocatorOne->method('has')->will($this->returnValueMap([
            ['someServiceOne', true],
            ['someServiceTwo', false],
            ['someService', false]
        ]));
        $serviceLocatorOne->method('get')->will($this->returnValueMap([
            ['someServiceOne', 'serviceInstanceOne'],
        ]));
        $builderOne
            ->expects($this->once())
            ->method('configure')
            ->willReturn($serviceLocatorOne);
        $builderOne
            ->expects($this->once())
            ->method('build')
            ->willReturn($serviceLocatorOne);

        $builderTwo = $this->getMockBuilder(ExternalContainerBuilderInterface::class)->getMock();
        $packageTwo
            ->expects($this->once())
            ->method('loadExternalContainerBuilder')
            ->with($packages)
            ->willReturn($builderTwo);

        $serviceLocatorTwo = $this->getMockBuilder(ServiceLocatorInterface::class)->getMock();
        $serviceLocatorTwo->method('has')->will($this->returnValueMap([
            ['someServiceOne', true],
            ['someServiceTwo', true],
            ['someService', false]
        ]));
        $serviceLocatorTwo->method('get')->will($this->returnValueMap([
            ['someServiceOne', 'serviceInstanceOneFromTwoContainer'],
            ['someServiceTwo', 'serviceInstanceTwo'],
        ]));
        $builderTwo
            ->expects($this->once())
            ->method('configure')
            ->willReturn($serviceLocatorTwo);
        $builderTwo
            ->expects($this->once())
            ->method('build')
            ->willReturn($serviceLocatorTwo);

        $loader = new ContainerLoader();
        $container = $loader->loadFromPackagesConfiguration($packages);

        $this->assertTrue($container->has('someServiceOne'));
        $this->assertTrue($container->has('someServiceTwo'));
        $this->assertFalse($container->has('someService'));

        // Becouse priority
        $this->assertEquals('serviceInstanceOne', $container->get('someServiceOne'));
        $this->assertEquals('serviceInstanceTwo', $container->get('someServiceTwo'));
    }
}