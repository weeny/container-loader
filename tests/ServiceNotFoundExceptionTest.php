<?php

namespace Weeny\Core\ContainerLoader\Tests;

use PHPUnit\Framework\TestCase;
use Weeny\Core\ContainerLoader\Exceptions\ServiceNotFoundException;

class ServiceNotFoundExceptionTest extends TestCase
{

    /**
     * @dataProvider dataProvider
     */
    public function testGetServiceName($expectedName)
    {
        $exception = new ServiceNotFoundException($expectedName, 'Some error');
        $this->assertEquals($expectedName, $exception->getServiceName());
    }

    public function dataProvider()
    {
        return [
            ['SomeServiceOne'],
            ['SomeServiceTwo']
        ];
    }
}